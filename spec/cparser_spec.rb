# frozen_string_literal: true
$:.unshift(File.expand_path('../ext', __dir__))
require 'cparser'

describe GitParser do
  real_commit = <<~EOC
    tree b6444fbbdb3a8b983016cffd2380c67d7c97b6f3
    parent bdb9ca55b37033be958880c407ede93621d4bea1
    author Jacob Vosmaer <contact@jacobvosmaer.nl> 1633856881 +0200
    committer Jacob Vosmaer <contact@jacobvosmaer.nl> 1633856881 +0200

    Namespacing
  EOC
  tree_line = "tree b6444fbbdb3a8b983016cffd2380c67d7c97b6f3\n"
  parent_line = "parent bdb9ca55b37033be958880c407ede93621d4bea1\n"
  author_line = "author Jacob Vosmaer <contact@jacobvosmaer.nl> 1633856881 +0200\n"
  committer_line = "committer Jacob Vosmaer <contact@jacobvosmaer.nl> 1633856881 +0200\n"

  commit_examples = [
    [real_commit, {
      tree: 'b6444fbbdb3a8b983016cffd2380c67d7c97b6f3',
      parents: ['bdb9ca55b37033be958880c407ede93621d4bea1'],
      author: {name: 'Jacob Vosmaer', email: 'contact@jacobvosmaer.nl', date: 1633856881, timezone: '+0200'},
      committer: {name: 'Jacob Vosmaer', email: 'contact@jacobvosmaer.nl', date: 1633856881, timezone: '+0200'},
      message: "Namespacing\n"
    }],
    [tree_line + parent_line * 2 + author_line + committer_line + "\nhello", {
      tree: 'b6444fbbdb3a8b983016cffd2380c67d7c97b6f3',
      parents: ['bdb9ca55b37033be958880c407ede93621d4bea1', 'bdb9ca55b37033be958880c407ede93621d4bea1'],
      author: {name: 'Jacob Vosmaer', email: 'contact@jacobvosmaer.nl', date: 1633856881, timezone: '+0200'},
      committer: {name: 'Jacob Vosmaer', email: 'contact@jacobvosmaer.nl', date: 1633856881, timezone: '+0200'},
      message: "hello"
    }],
    [tree_line + author_line + committer_line + "\nhello", {
      tree: 'b6444fbbdb3a8b983016cffd2380c67d7c97b6f3',
      parents: [],
      author: {name: 'Jacob Vosmaer', email: 'contact@jacobvosmaer.nl', date: 1633856881, timezone: '+0200'},
      committer: {name: 'Jacob Vosmaer', email: 'contact@jacobvosmaer.nl', date: 1633856881, timezone: '+0200'},
      message: "hello"
    }],
  ]
  author_examples = [
    ["aaa  \n", {name: 'aaa  '}],
    ["aaa<  \n", {name: 'aaa<  '}],
    ["aaa<bbb>   \n", {name: 'aaa', email: 'bbb'}],
    ["aaa <bbb>\n", {name: 'aaa', email: 'bbb'}],
    ["aaa  <bbb>\n", {name: 'aaa', email: 'bbb'}],
    ["aaa <bbb>123\n", {name: 'aaa', email: 'bbb', date: 123}],
    ["aaa <bbb> 123\n", {name: 'aaa', email: 'bbb', date: 123}],
    ["aaa <bbb> 123 +4567\n", {name: 'aaa', email: 'bbb', date: 123, timezone: '+4567'}],
    ["aaa <bbb> 123 -4567\n", {name: 'aaa', email: 'bbb', date: 123, timezone: '-4567'}],
    ["aaa <bbb> 123+4567\n", {name: 'aaa', email: 'bbb', date: 123, timezone: '+4567'}],
    ["aaa <bbb> 123 +4567zzz\n", {name: 'aaa', email: 'bbb', date: 123, timezone: '+4567'}],
    ["aaa <bbb> 123 +456\n", {name: 'aaa', email: 'bbb', date: 123}],
    ["aaa <bbb> 123 +45678\n", {name: 'aaa', email: 'bbb', date: 123}],
    ["aaa <bbb> +4567\n", {name: 'aaa', email: 'bbb'}],
  ]
  real_tag=<<~EOT
    object 2c3eb9d8e549b7a31ac962e715f4555c03d0bd2d
    type commit
    tag tag1
    tagger Jacob Vosmaer <contact@jacobvosmaer.nl> 1633780413 +0200
    
    this is a tag
  EOT
  tag_examples = [
    [
      real_tag, 
      {
        object: '2c3eb9d8e549b7a31ac962e715f4555c03d0bd2d',
        type: 'commit',
        tag: 'tag1',
        tagger: {name: 'Jacob Vosmaer', email: 'contact@jacobvosmaer.nl', date: 1633780413, timezone: '+0200'},
        message: "this is a tag\n",
      }
    ]
  ]

  describe '.parse_commit' do
    commit_examples.each do |input, output|
      it(input) { expect(GitParser.parse_commit(input)).to eq(output) }
    end
  end

  describe '.parse_author' do
    author_examples.each do |input, output|
      it(input) { expect(GitParser.parse_author(input)).to eq(output) }
    end
  end

  describe '.parse_tag' do
    tag_examples.each do |input, output|
      it(input) { expect(GitParser.parse_tag(input)).to eq(output) }
    end
  end
end
